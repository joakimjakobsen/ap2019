var cir1 = 0;
var cir2 = 0;
var cir3 = 0;
var cir4 = 0;

var words = ['10','9','8','7','6','5','4','3','2','wait','.','..','...'];
var index = 0


function setup(){
createCanvas(windowWidth,windowHeight);
background(255);

}

function draw(){

rectMode(CENTER);
translate(width/2,height/2);



cir1 = cir1 + 2;
rotcurve(cir1,200, 0, 230, 230);
cir2 = cir2 - 2.2;
rotcurve(cir2, 150, 0, 230, 230);
cir3 = cir3 + 1.8;
rotcurve(cir3,100, 0, 230, 230);
cir4 = cir4 - 1.5;
rotcurve(cir4, 50, 0, 230, 230);




push();
textAlign(CENTER);
text(words[index],0,2);
if (frameCount%40==0){
fill(0)
index = index + 1;
}

  if (index >= words.length) {
    index = 0;
}
pop();

}

function rotcurve(cir,a,r,g,b){ //function that makes the circle+square rotate

push();
rotate(radians(cir));
  noFill();
stroke(r,g,b);
strokeWeight(3);
ellipse(0,0,a,a);
fill(255)
strokeWeight(5);
stroke(255);
rect(0,0,a,30);
pop();

}
