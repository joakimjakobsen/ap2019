![Screenshot](Skærmbillede_2019-02-23_kl._14.10.22.png)


*Describe about your throbber design, both conceptually and technically.*

My throbber concept was to make two half circles rotate and then every other would rotate the opposite way of the previous. In the middle I have a sort of counter counting down from 10 but when it reaches 2 it says wait and then counts from 10 again. I started out trying to make to half circles with curveVertex(). That did not work so well so I came up with the idea of creating a white square that overlapped a circle with noFill () and a blue stroke. 

*What are the time-related syntaxes/functions that you have used in your program? and why you use in this way? How is time being constructed in computation (can refer to both reading and your process of coding)?*

My time related syntaxes are how fast the different ellipses are turning and the counter in the middle. The speed of the ellipses is altered by different variables that put in numbers of the rotate(radians( ___ ) ) syntax. The counter is done with an if-function. 

When the circles rotate, it gives a feeling of an ongoing flow. The counter builds up a sense of suspense, that drops because the counter keeps starting over. My throbber gives the user a false hope of that something is about to happen.  

Time in computation is measured by the time between two pulses. The time between the change of a 0 to a 1 and back to 0 again. The time between these are so small that the human cannot perceive it. That’s why it is called micro temporal. 

*Think about a throbber that you have encounted in digital culture e.g streaming video on YouTube or loading latest feeds on Facebook or waiting a ticket transaction, what do you think a throbber tells us, and/or hides, about? How might we think about this remarkable throbber icon differently?*

Last I encountered a throbber where I thought about its present was when I played a game on my Playstation. It appeared when I wanted to go from one game mode to another. I think the throbber is a way to tell us that something is in progress and our action soon will have an effect. It is there to give us feedback on our actions so that we know that the program has registered our action, but it needs some time to make the wished command happen.


RUNME: https://cdn.staticaly.com/gl/joakimjakobsen/ap2019/raw/master/Mini_ex_3/p5/miniex/index.html 