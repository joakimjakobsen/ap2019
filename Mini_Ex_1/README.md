![ScreenShot](Mini_Ex_1/p5/miniex/boobou.png)

**Describe your first independent coding process (in relation to thinking, reading, copying, modifying, writing, uploading, sharing, commenting code)**

The point of this thing/monster called Boobou is that you can steer its mouth and body with your mouse and make it look like he it is talking. 

My first thought when doing this program was that I wanted to explore the mouseX and use it to control how both the x-coordinate, y-coordinate, width and height acted. Then I got the idea that it could look like a mouth so I added two white ellipses as eyes and made them follow the mouse as well but with the x and y a bit of axis so that the eyes appeared above and to left and right of the mouth. I then added two small black ellipses as pupils. I then tried to make a tooth inside the mouth but it turned out to look like a mouth on a body/head instead. Then I added the text.

When doing the program, I constantly tried different numbers to see what the changes would be. Also, I read the references in the p5 library to try and figure out what did what. 


**How your coding process is different or similar to reading and writing text? (You may also reflect upon Annette Vee's text on coding literacy)**

I think my coding process is like reading and writing a text in that way that when you compose a text you first write something, then read what you have written and then you can alter your text so it fulfils your wishes. The same counts for coding. First you try and write a code. If it doesn’t do what you wished it to you can afterwards try and alter it so that it does what you want. The difference is though that the human reader often gets the meaning of a text even though it might contain spelling errors. If you make spelling errors in your coding the computer cannot understand what you want it to do. 


**What is code and coding/programming practice means to you?**

I find coding quite fun because it is easy to play with different things and when it works for you it feels like a great accomplishment. 


RUNME: https://glcdn.githack.com/joakimjakobsen/ap2019/raw/master/Mini_Ex_1/p5/miniex/index.html 
