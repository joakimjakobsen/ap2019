function setup() {
  createCanvas(500, 500);
}

function draw() {

  background(0,0,0); //gør at baggrunden hele tiden bliver tegnet på ny
  textSize(50);
 	fill(0, 102, 153);
	text('This is Boobou', 10, 60);


  noStroke();
  fill(color(51, 133, 255));
	ellipse(mouseX,mouseY,mouseX,mouseY); //body
  fill(color(255,255,255));
	ellipse(mouseX-23,mouseY-60,mouseX-150,mouseY-150); //mouth

  fill(color(255,255,350));
  ellipse(mouseX-90,mouseY-130,50,50); //left eyeapple
  fill(color(255,255,350));
  ellipse(mouseX+90,mouseY-130,50,50); //right eyeapple
  fill(color(0,0,0));
  ellipse(mouseX-90,mouseY-130,20,20); //left pupil
  fill(color(0,0,0));
  ellipse(mouseX+90,mouseY-130,20,20); //right pupil

}
