function setup() {
	createCanvas(700, 700);

}

function draw(){
background(200,240,255);

	push()
	fill(255,255,0);
	noStroke();
	ellipse(350, 350, 350,350);//body

	fill(color(0));
	ellipse(290,300,20,20); //left pupil
	fill(color(0));
	ellipse(410,300,20,20); //right pupil
	pop()

//mouth
	strokeWeight(10);

	noFill();
	beginShape();
	console.log(mouseY);
	curveVertex(260, mouseY);
	curveVertex(440, 400);
	curveVertex(260, 400);
	curveVertex(440, mouseY);
	endShape();

//titles
	textFont('comicSansMs');
	textSize(100)
	fill(0);
	text('FRIDAY!', 150, 90);
	text('monday...',150,660);

}
