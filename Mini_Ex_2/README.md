![Screenshot](Skærmbillede_2019-02-15_kl._12.30.13.png)

**Sketch.js (monday or friday)** 
*Describe your program and what you have used and learnt*

My program is a smiley where you can alter its mood anywhere between happy and sad/mad depending on where your mouse is. I have used and learned some new syntaxes. The strokeWeight, curveVertex, beginShape, Endshape and textFont were all new things I tried out for this program.

*What have you learnt from reading the assigned reading? *

I have learned that programs can have quite an impact on the society and it is important to consider your program carefully before releasing it to the public use. As shown in the text, when Apple added the modifier to the emoji they in there try to make the emoji more diverse ended up actually offending and excluding more people. It had the quite opposite effect oppose to what they expected. Abbing et al. thinks that they acted too quickly and the modifier implementation was too rushed. 

*How would you put your emoji into a wider cultural context that concerns identification, identity, race, social, economics, culture, device politics and beyond? (Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts - this is a difficult task, you may need to spend sometimes in thinking about it)*

My emoji is more of a fun gimmick that shows how most people feel about the days Friday and Monday. Without the text though it could be a more nuanced way of showing your current mood. If you’re not totally glad but still pretty glad you now have the option to make an emoji that shows the in between emotions. This program could also open the idea that you with your finger or mouse could change different aspects of the emoji so it becomes more personal. Another 

RUNME:https://cdn.staticaly.com/gl/joakimjakobsen/ap2019/raw/master/Mini_Ex_2/p5/miniex/index.html

![Screenshot](Skærmbillede_2019-02-15_kl._12.39.15.png)

**Sketch2.js (THE IDEAL EMOJI)**

*Describe your program and what you have used and learnt *

This emoji is the most neutral/boring/plain mood I could think of. I have used and learned a thing with this one as well. I found out that you with the random(__,__) are able to make the colors shift in a color range and not just all the colors. I have made the body of the smiley only “blink” a little so it wasn’t too disturbing and the eyes + mouth blink a lot with the whole color range of green.

*How would you put your emoji into a wider cultural context that concerns identification, identity, race, social, economics, culture, device politics and beyond? (Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts - this is a difficult task, you may need to spend sometimes in thinking about it)*

The idea of this emoji came to me when I was reading Abbing et al.’s text. For me it sounded like nothing was good enough for her and what she ultimately wanted was just one plain neutral smiley so that no one felt left out or discriminated since no one could identify them with it. 

RUNME: https://cdn.staticaly.com/gl/joakimjakobsen/ap2019/raw/master/Mini_Ex_2/p5/miniex/index2.html