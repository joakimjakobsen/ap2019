![Screenshot](Skærmbillede_2019-03-25_kl._09.17.48.png)


Version 1.0 : https://cdn.staticaly.com/gl/joakimjakobsen/ap2019/raw/master/Mini_ex_7/p5/miniex/index.html

Version 2.0 : https://glcdn.githack.com/joakimjakobsen/ap2019/raw/master/Mini_ex_7/p5/miniex/index.html (the canvas is stretched so you can see how they break out of the original canvas after some time and the transparency is lower)


**What are the rules in your generative program and describe how your program performs over time. What have been generated beyond just the end product?**

There are a total of 4 rules. One for each side of the canvas it overlaps or hits. The random numbers of b2 and b1 redefines the speed and direction for the outer point of each line when hitting the canvas sides. Also they either add 0 to 255 red or blue to the color of the line. If a line hits the left side of the canvas a random stroke weight between 1 and 7  is given at the moment of the outer point being x>=0. What is generated is a star like substance that increases in volume and speed over time. It is out of the hands of the creator to know how fast it will evolve and how each line will act through the time of the program. The rules make the program seem lively and explosive.

**What's generativity and automatism? How does this mini-exercise help you to understand what might be generativity and automatism? (see the above - objective 3 and the assigned readings)**

Generativity and automatism is when the program can produce something more or less unpredictable by having a set of rules to run on and some kind of randomness implemented in the code. In other words: “Generative art refers to any practice where the artist uses a system such as a set of natural language rules, a computer system, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art.” (Phillip Galanter)

This miniex helped us conceptualize the terms generativity and automatism because we had to make one ourselves and for us to answer the task correctly we had to read through our notes and discuss the terms with each other. 

