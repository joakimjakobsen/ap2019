var lines = [];
var xoff = 0;

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(35);
  // make 100 lines in the middle with a different x2 and y2 and random direction
  for (let i=0; i<80; i++){
    lines[i]  = new Line(200, 200, random(400), random(400), random(-1,1),random(-1,1));
  }
}

function draw() {
    background(0,9);
    //draw the lines
  for (let i=0; i<lines.length; i++){
  lines[i].show();
  lines[i].move();
  }

}


class Line {
  constructor(x1, y1, x2, y2,b1,b2){
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    this.b1 = b2
    this.b2 = b2
    this.r = 0;
    this.b = 0;

  }
  show(){
  //color of the lines
  xoff = xoff + 0.02;
    let n = noise(xoff) * 350;
  stroke(this.r,650-n*0.8,this.b);
    line(this.x1,this.y1, this.x2, this.y2);

  }

  move(){
    this.x2 += this.b1;
    this.y2 += this.b2;
    //rules that changes the direction and increases speed when hitting one of the 4 wall of the canvas
    if (this.x2 <= 0) {
      this.b1 += random(5);

      strokeWeight(random(1,7))

      if(random(1) < 0.5){
        this.r = random(255);
      } else {
      this.b = random(255);
      }
    }
    if (this.y2 <=0 ){
      this.b2 += random(5);
        strokeWeight(random(1,7))
      if(random(1) < 0.5){
        this.r = random(255);
      } else {
      this.b = random(255);
      }
    }
    if (this.x2 >=400 ){
      this.b1 += random(-5);
        strokeWeight(random(1,7))
      if(random(1) < 0.5){
        this.r = random(255);
      } else {
      this.b = random(255);
      }
    }
    if (this.y2 >=400 ){
      this.b2 += random(-5);
        strokeWeight(random(1,7))
      if(random(1) < 0.5){
        this.r = random(255);
      } else {
        this.b = random(255);
      }
    }
  }
}
