var ctracker;
var positions;

function setup() {
  background(100);
  //web cam capture
  var capture = createCapture();
  capture.size(640,480);
  capture.position(0,0);
  //capture.hide();
  var c = createCanvas(640, 480);
  c.position(0,0);

  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);


  // unsuccesful foreloop for buttons :/
  // for (var x = 0; x <= 680; x = x + 100) {
  //   for (var y = 0; y <=480; y = y + 100) {


  // setup button
  button1 = new button(100,250);
  button2 = new button(100,400);
  button3 = new button(300,250);
  button4 = new button(300,400);
  button5 = new button(500,250);
  button6 = new button(500,400);
  button1.begin();
  button2.begin();
  button3.begin();
  button4.begin();
  button5.begin();
  button6.begin();
  console.log('yo')

  // textbox header

  rectMode(CENTER);
  noStroke();
  rect(320,55,400,70,10);
  push();
  textSize(50);
  textAlign(CENTER);
  textStyle(BOLD);
  textFont('sanFransisco');
  fill(66,103,178);
  text('Capture them all!', 320, 70);
  pop();

  //textbox info
  fill(255);
  noStroke();
  rect(320,485,250,50,5)
  fill(0);
  textAlign(CENTER);
  text('Use your mouth to get the likes',320,475);


}

function draw() {

  positions = ctracker.getCurrentPosition();
  if (positions.length) {

    button1.gone();
    button2.gone();
    button3.gone();
    button4.gone();
    button5.gone();
    button6.gone();
    console.log("halløj")
  }
}


class button{
  constructor(_xpos, _ypos){
    this.xposition = _xpos;
    this.yposition = _ypos;

    //bounds
    this.minx = this.xposition - 40;
    this.maxx = this.xposition + 40;
    this.miny = this.yposition - 40;
    this.maxy = this.yposition + 40;
  }
  // make a button
  begin(){
    this.button = createButton('like');
    this.button.position(this.xposition, this.yposition);
    this.button.style("display","inline-block");
    this.button.style("color","#fff");
    this.button.style("padding","5px 8px");
    this.button.style("text-decoration","none");
    this.button.style("font-size","0.9em");
    this.button.style("font-weight","normal");
    this.button.style("border-radius","3px");
    this.button.style("border","none");
    this.button.style("text-shadow","0 -1px 0 rgba(0,0,0,.2)");
    this.button.style("background","#4c69ba");
    this.button.style("background","-moz-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
    this.button.style("background","-webkit-gradient(linear, left top, left bottom, color-stop(0%, #3b55a0))");
    this.button.style("background","-webkit-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
    this.button.style("background","-o-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
    this.button.style("background","-ms-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
    this.button.style("background","linear-gradient(to bottom, #4c69ba 0%, #3b55a0 100%)");
    this.button.style("filter","progid:DXImageTransform.Microsoft.gradient( startColorstr='#4c69ba', endColorstr='#3b55a0', GradientType=0 )");


  }
  // make the button disappear if its inside the minx, maxx, miny and maxy.
  gone(){
    if (positions[60][0] > this.minx && positions[60][0] < this.maxx && positions[60][1] > this.miny && positions[60][1] < this.maxy){
      this.button.remove();
      console.log("dav")
    }

  }
}
