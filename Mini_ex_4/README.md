![Screenshot](Skærmbillede_2019-03-03_kl._16.16.48.png)

**Describe about your sketch and explains what have been captured both conceptually and technically. How's the process of capturing?** 

My sketch is about capturing Facebook likes and it is the goal to capture them all so that none is left. Technically it is about capturing the position of the upper lip to determine when the like button should disappear from the screen. I have used an if function and made variables for the x and y positions to make the program remove the button when the position of the lip is inside a certain area. Conceptually it is a comment on how we without any further due just focuses on getting and clicking the likes instead of worrying about how Facebook can track our every move. This is what the tracking of the lip symbolizes. You do not consider that there is a program that is able to recognize your face, your focus lies on the game and your goal. 


**Together with the assigned reading and coding process, how might this ex helps you to think about or understand the data capturing process in digital culture?**

I was surprised to learn that the only thing Facebook needed to track one’s movement on other sites was them to have a social plugin. Most of the things mentioned in the text I was familiar with though and I knew that the capturing of one’s data is only growing and the data is used by large and small businesses to direct advertisement and to analyse what people like and use it to improve their products. 

RUNME: https://cdn.staticaly.com/gl/joakimjakobsen/ap2019/raw/master/Mini_ex_4/p5/miniex/index.html  