Flowchart: https://6brwpc.axshare.com

Based on Mini_ex_6: https://gitlab.com/joakimjakobsen/ap2019/tree/master/Mini_ex_6

**The difficulty of making the flowchart:**

When doing the flow chart I ran into different difficulties. The most challenging one was to figure out in which order the different things should be arranged in for it to make sense. Also I constantly thought I was done but then I found some loose end that just stopped with a processing box. It was also difficult to translate the code into something that would be easier to follow when reading it in a flow chart.

Idea one: 

![Screenshot](Goalflow.png)

Idea two:

![Screenshot](ballflow.png)

For this week we were assigned to develop two flowcharts in the group. In this phase the point was not to come up with super realistic ideas or super technical ideas either. The point was to try to create flowcharts without having written the code yet. Thus it should be more simple for us when we start coding, since the program is already sketched and divided into steps.


A challenge will be for us to carry out our ambitions for these flowcharts. Especially the flowchart of the game will have several obstacles along the way. First of all we will probably face difficulties when creating our avatar, as we wish to capture a picture of the user so the user becomes the avatar in the game. To make this happen we must make use of our knowledge from the theme of data capturing using the p5.dom library. We must use a face tracker and identify which parts of the face we want to incorporate in the avatar. It is important that the face moves along with the avatar throughout the whole game.
Another challenge will be to make a magnetic space between the “star”, which symbolises the expectations we meet during our youth, and the avatar, pulling the avatar towards the star. We will try to solve this by making a transparent square around the star and if the avatar is inside this square it will hit the expectations and you will lose happiness in the “happy-meter”. There is some sort of attractive force. This force will make it more difficult for you to reach your life-goals and gain happiness, since expectations will pull you over wherever you go in the arena and lower your happiness if you hit it.
It might also be quite a challenge for us to create a “happy-meter” which keeps lowering when you don’t reach life-goals. We want to lower it by approximately 5% for every five seconds you don’t collect a life-goal to make it more realistic and similar to real life.


Our second flowchart shows a more simple  program containing parts we have worked with before. The main challenge will be to continuously query a new word without having to write them over and over again. Also this word has to match with a gif from the API which in this case is GIPHY. Another challenge is how the ball should move around and we will try solving this by looking at one of our colleagues’ codes, and hopefully we can use the same method.