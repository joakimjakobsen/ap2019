let tofu = [];

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);
}

function draw() {
  background(30);
  for (let i=0; i< tofu.length; i++){
    tofu[i].move();
    tofu[i].show();
  }
}

function mousePressed() {
  addtofu(mouseX,mouseY);

}


function addtofu (MX, MY) {
tofu.push(new Tofu(floor(random(3,30)), MX, MY, floor(random(30,40))));

}



class Tofu () {
  constructor(speed, xpos, ypos, size) {
    this.speed = speed;
    this.pos = new createVector(xpos, ypos); // same as this.xpos = thi
    this.size = size;
  }

  move(){
    this.pos.x += this.speed;
    if (this.pos.x > width + 5) {
      this.pos.x = 0;
    }
  show() {
    fill(0,200,0);
    rect(this.pos.x, this.pos.y, this.size, this.size);
  }

}
