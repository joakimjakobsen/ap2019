var taco;
var imgk;
var score = 0;
var koriander = [];

function preload() {
  imgk = loadImage('sprite_0.png');
  bord = loadImage('bord0.png');
  gameover = loadImage('gameover.png');
  gamefont = loadFont('kongtext.ttf');
}

function setup() {
  createCanvas(600,500);
  frameRate(30);

//setup the taco
  taco = createSprite(200,450);
  taco.addAnimation('run','tacorunn0.png','tacorunn1.png');
  taco.addAnimation('stand', 'tacostand0.png','tacostand1.png')
  taco.scale = 1.5

//setuo the coriands
  for (let i = 0; i < 200; i++){
    let x = random(-5,600)+ random(-10,10);
    let y = random(-6000,0);
    koriander[i] = new Koriand(x, y, 5);
  }
}

function draw() {
  background(230, 255, 153)
  image(bord,0,100,610,500);

//engage the coriands
  for (let i = 0; i < koriander.length; i++){
    koriander[i].move();
    koriander[i].show();
    koriander[i].gone();
  }

// enable the taco to move
  if(keyIsDown(RIGHT_ARROW)){
    taco.changeAnimation('run');
    taco.velocity.x = 5;
    taco.mirrorX(1);
  }
  else if(keyIsDown(LEFT_ARROW)){
    taco.changeAnimation('run');
    taco.velocity.x = -5;
    taco.mirrorX(-1);
  }
  else {
    taco.changeAnimation('stand');
    taco.velocity.x = 0;
  }

// constrain the taco
  if (taco.position.x <= 30){
    taco.position.x = 30;
  }

  if (taco.position.x >= 570){
    taco.position.x = 570;
  }

  drawSprites();
  textboxes();
  if ( score == 200){
    push();
    textFont(gamefont);
    textSize(50);
    textAlign(CENTER);
    text('YOU WIN!',300,250);
    pop();
    
  }
}

function textboxes(){
  textFont(gamefont);
  text('SCORE:' + score*50,450,50);


}

class Koriand {
  constructor(x, y, speed){
    this.x = x;
    this.y = y;
    this.speed = speed;
    this.splat = false;
    this.dead = false;
  }

  move() {
    this.y = this.y + this.speed;
  //when the coriands hit the table
  //they go away and you get 50 scorepoints
  if (this.y >= 405 && this.splat==false){
    this.splat = true;
    score++;
    this.y = 400
    this.x = -500
    }
  }

  show() {
    this.image = image(imgk,this.x,this.y);

  }

  gone() {
  if(this.x > taco.position.x-85 && this.x < taco.position.x-1 && this.y > taco.position.y-70 && this.y < taco.position.y-19) {
    this.speed = 0
    this.dead = true
  }
    if (this.dead == true){
      push();
      textFont(gamefont);
      textSize(50);
      textAlign(CENTER);
      text('GAME OVER!',300,250);
      pop();
      noLoop();
    }
  }
}
