![Screenshot](Skærmbillede_2019-03-16_kl._20.19.57.png)


RUNME: https://glcdn.githack.com/joakimjakobsen/ap2019/raw/master/Mini_ex_6/p5/miniex/index.html

**Describe how does your game/game object work?** 

You steer the taco from side to side with the arrow keys and the goal is to avoid getting coriander in the taco. For each coriander leaf you miss 50 score points is given. The max score is 10,000 and you have won the game if you get this. I could for some reason not get the if statement to work with the score, but I wanted it to have said “You win!”. 


**Describe how you program the objects and their related attributes and methods in your game.**

I used createSprite to do the taco and class for the coriander. In the class for coriander I made 3 functions. One that is about how the object should look called show. Here I used the image function. In the second function, I define which movements it should be able to do. It should fall and go away when it hits the table. And in the third function I make an if statement that checks and acts if the coriander hits the taco. The hit box is not so accurate because it is a square and not the exact shape of the taco which may frustrate some. I have though tried to make it so it is as fair as possible and sometimes the hit box is also forgiving. 

The taco is programmed in the draw function with if statements for it to change the animation with “changeAnimation” and direction with “mirrorX” when a key or no key is pressed. It is also constrained within the walls of the canvas so you cannot cheat. For the taco to move velocity.x is used for it to add numbers to the x coordinate of the taco. 


**Based on Shiftman's videos, Fuller and Goffey's text and in-class lecture, what are the characteristics of object-oriented programming?** 

When you do object-oriented programming, you organize your code by defining objects and applying data to these objects that can be encapsulated. It is a form of abstraction that makes programming language easier to understand and work with. When doing object oriented programming you try to think about what you want to do as if it was actual objects and translate it into code. The objects also have some interactivity and you use the classes to act upon each other. 

**Extend/connect your game project to wider digital culture context, can you think of a digital example and describe how complex details and operations are being abstracted? What are the implications of using object oriented programming?**

I really do not think that I understand the first question but here is my guess. My game abstract the complex details by making it easy to understand that the arrow keys is used to make the taco move. But in fact, the user does not steer an actual object. It is only because of my coding and appliance of commands and data on self-drawn animations and pictures in the code that it can be moved and act like if it was an object. 

The implications of using object oriented programming can be that it is in the hand of the developers of the functions to determine what can and cannot be done with the code and what attributes the functions should have. 


