![Screenshot](Skærmbillede_2019-03-09_kl._16.34.20.png)


Run: https://glcdn.githack.com/joakimjakobsen/ap2019/raw/master/Mini_ex_5/p5/miniex/index.html 

**The concept, proces and expression of the program**

I have revisited my program from mini_ex 4 because I had some other ideas that was in the same alley but a bit different. This time I have left out the clmtrackr, I found it too difficult to work with so this time it was the mouse I wanted use as the thing being captured. This program is a game where you must capture the like just as my previous program but this time the like is moving and you must capture it with your mouse instead of the mouth. I started out wanting a button to move around. This I made with if statements which registers the x and y of the button and then a command to say what variable (x or y) that should be permanent and which variable that should go up by the speed. Then I wanted to make an if statement so that when the mouse is within a certain area and is pressed the buttons x and y will be at a position that is out of the canvas. And when this has happened a reset button should appear and the speed should go 1 up so that the button moves faster and it becomes more difficult to capture it. Also, it will add to a counter saying how many likes you have captured. When the button disappears the reset-button should appear and you can start over by clicking it.

With this program, I want to express more or less the same thing as I wanted for the previous one. The user of the program is focusing on getting the like and is so busy trying to get these likes that he/she does not realize that the mouse movement is actually being tracked by the program, and it knows exactly where on the screen it has been. I did not know how to actually record or data mine the mouse movement, but the idea was also that you in the code could see that it actually was sending the data to a foreign server.

**Based on the readings that you have done before (see below), What does it mean by programming as a practice?**

Programming as practice can be seen as the way or how people program and why they do it. Some use programming as a tool for making art and use it for critical design. Some use it to make programs for companies etc. In the text “the practices of programming” by Bergstrom, I. and Blackwell, A.F. (2016) they present six overall approaches to programming: Established software engineering practice, bricolage & tinkering, sketching with code, live coding, hacking and code-bending. Often the programmer will be able to identify having one or more of the above approaches when programming. 

**To what extend do you agree the concepts that have been illustrate in below readings? (Can you draw and link some of the concepts in the text and expand with your critical take?)**

I agree on most of the things we have read. In Coding for Everyone and the Legacy of Mass Literacy (Anette Vee, 2017) she writes about how she thinks that everyone should be taught programming in the early stages of school. She thinks that it is important for the new generation to have knowledge about programming since it becomes a more and more vital part of our everyday life. I see how it is important to do this, because the “power” then becomes more evenly distributed and it is not only the few who understands what is going on in the programs and technologies we use every day that have a say. Or more importantly that our politics surrounding technology becomes something that more people can understand and take a stand point in or make better solution propositions for. 

**What is the relation between programming and digital culture?**

Programming is in a way what shapes the digital culture. When programming you make certain things possible and the choices done within the code have a real impact on how the society uses the digital products that is constructed by code. Of cause the digital culture is also shaped around how we choose to use the products and in what way we talk about them and to what extend we influence each other’s behavior. 
