
var x = 200;
var y = 200;
var speed = [1,2,4,5,8,10];
var index1 = 0
var index2 = 0

function setup() {
  createCanvas(600,530);
  button = createButton('reset');
  button.position(280,285);
  button.hide();

}

function draw() {

  background(255, 176, 127)
  textboxes();
  fb(x,y);
  move(speed[index1]);
  button.mousePressed(reset);
  gone();

}

// Remove the like button Add one like to counter and Speed up and Show resetbutton
function gone(){
  if (mouseIsPressed && mouseX >= x-20 && mouseX <= x+20 && mouseY >= y-20 && mouseY <= y+20){
  x = -500
  y = -500
  index1++;
  index2++;
  }

  if (index1 == speed.length) {
  index1 = 5
  }

  if (x == -500 && y == -500){
    button.show();
  }
}

//make the like button come back to if cycle and the reset button disappear
function reset() {
  x=200
  y=200
  button.hide();
}

//like button design
function fb(x,y) {

  noStroke();
  fill(59, 89, 152)
  rect(x,y,60,20,5);
  fill(255);
  text('Like', x, y+5);

}

//makes the button go around
function move (speed) {

  if (x >= 200 && y==200){
    x = x + speed
  }
  if (x == 400 && y>=200){
    x = 400
    y = y + speed
  }
  if (x <= 400 && y>=400){
    y = 400
    x = x - speed
  }
  if (x <= 200 && y<=400){
    x = 200
    y = y - speed
  }
}

function textboxes(){

//textboxheader
rectMode(CENTER);
noStroke();
rect(300,55,400,70,10);
push();
textSize(50);
textAlign(CENTER);
textStyle(BOLD);
textFont('sanFransisco');
fill(66,103,178);
text('Capture the like!', 300, 70);
pop();

// like counter

fill(255);
noStroke();
rect(300,110,50,20,5)
fill(0);
textAlign(CENTER);
// text('likes:',280,125)
text('Likes: '+[index2],300,115);

//textbox info
fill(255);
noStroke();
rect(300,485,250,50,5)
fill(0);
textAlign(CENTER);
text('Use your mouse to get the like',300,490);
}
