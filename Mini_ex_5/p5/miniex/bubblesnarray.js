let bubbles = [];

function setup() {
  createCanvas(600, 400);
  for (let i = 0; i < 150; i++){
    let x = 10 + 30 *i;
    let y = random(500)
    bubbles[i] = new Bubble(x,y,5);
  }
}

function draw() {
  background(100,100,250);
  for (let i = 0; i < bubbles.length; i++){
    bubbles[i].move();
    bubbles[i].show();
  }

}

class Bubble {
  constructor(x, y, r){
    this.x = x;
    this.y = y;
    this.r = r;
  }

  move() {
    this.x = this.x + random(-5,5);
    this.y = this.y +  random(-5,5);
  }

  show() {
    stroke(255);
    strokeWeight(2);
    noFill();
    ellipse(this.x, this.y, this.r * 2);
  }
}
