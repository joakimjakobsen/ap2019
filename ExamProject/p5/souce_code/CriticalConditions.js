/*Terms and conditions code by Charlotte Daugaard, Joakim Jakobsen, Mattias Johansen and Mie Andersen
Code inspired by regular terms and conditions with a twist*/

var gate = false;                   //defining gate to false to prevent gate from immediately running when opening the program
var audiolevel = 0.3;               //defining start volume to be 0.3
var roll = 1;                       //defining the increasing value that makes the text roll
var speed = 0.21;                   //defining the speed of the rolling text; how much the roll value is increased

function preload() {
  bg1 = loadImage('backgroundImg1.png');            //loading the background picture 1
  bg2 = loadImage('backgroundImg2.png');            //loading the backround picture 2
  speech = loadSound('Termsandconditions.wav');     //loading the sound
  tnc = loadStrings('text_01.txt');                 //loading text file
}

function setup() {
  cnv = createCanvas(1000,570);
  cnv.mouseMoved(activate);         //calls function activate if the mouse is within canvas
  background(bg1);                  //calling the first background picture

  //button setup
  button = createButton('Accept');  //creating button with the text 'Accept'
  button.position(655,440);         //choosing button position
  button.size(60,20);               //choosing button size
  button.hide();                    //Button is hidden until needed
}

function activate(){
  gate = true;                      //when function activate is run then gate is true
}

function draw() {
  if (gate == true){                //when gate is true then run function popUp();
    popUp();
  }
}

function popUp() {

  button.show();                    //shows button in the bottom right corner

  //sound setup
  speech.playMode('untilDone');     //make sure the sound does not play over it self
  speech.setVolume(audiolevel);     //sets the starting level of volume
  masterVolume(audiolevel);         //makes it possible to change the volumelevel
  speech.play();                    //initiates the sound file
  button.mouseClicked(volumeUp);    //if button 'Accept' is pressed then function volumeUp begins


  //rollingtext
  fill(255);                        //filling inner rectangle white behind text
  rect(155,180,560,260);            //creating the inner rectangle text
  textSize(20);
  textAlign(LEFT);                  //the text lines up with the left margin
  fill(0);                          //makes the text black
  text(tnc, 300, 300-roll,400);     //puts in the text from the textfile and substracts "roll" from the y-position to make the text move
  roll += speed;                    //adds speed to roll each time trough draw

  if (roll > 2500){                 //when the text is at a y position so high that only the endtext can be seen following should be executed
    speed = 0;                      //the text stops going up
    audiolevel = 0;                 //the soundfile stops
    button.mouseClicked(done);      //when mouse is clicked run function done
  }


  //background
  background(bg2);                  //calling the second background picture

  //creating popup box
  push();
  noFill();
  stroke(255);
  strokeWeight(70);
  rectMode(CENTER);                 //the x and y is now the center of the rectangle
  rect(500,300,500,300);            //creating the outer rectangle


  fill('#d9d9d9');                  //defining light grey color for rectangle
  noStroke();
  strokeWeight(0.5);
  rect(500,138,570,45);             //creating the light grey rectangle in the top part of the outer rectangle

  noFill();
  stroke('#737373');                //defining grey color for stroke
  rect(500,327,620,424);            //outer grey stroke
  rect(500,301,430,233);            //creating the inner grey stroke
  pop();


  //text for popup
  textSize(20);
  textAlign(CENTER);                //centers the text
  textFont('Times New Roman');
  text('Please read the terms and conditions',500,143);
}

function volumeUp(){                //volume turns up
  audiolevel += 0.03;               //adds 0.03 to audiolevel each time through draw
}

function done(){
    background(bg1);                //displays background 1
    button.hide();                  //hides button
    noLoop();                       //stops the mouseMoved loop
}
